package com.example.contactapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.contactapp.model.Contact
import com.example.contactapp.ui.theme.ContactAppTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BuildLayout()
        }
    }
}

@Composable
private fun BuildLayout() {
    var contactDisplayed by remember { mutableStateOf(Contact("Teste de nome")) }
    ContactAppTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            Column(Modifier.fillMaxWidth()) {
                ProfileCard(contactDisplayed)
                var contactList = mutableListOf<Contact>(
                    Contact("Fulano da Silva","lfksdhgjhglgmfdçgkç","34991662346","Rua Nao sei mais"),
                    Contact("Fulano Junior","ilghsliikjfsdf","34991662346","Rua Tudo imaginario"),
                    Contact("Fulano Jubileu","çfojaçfjghf","34991662346","Rua Sem ideia"),
                    Contact("Fulano Clausto","KHJAWLRJFDSF","34991662346","Rua Clanny"),
                    Contact("Fulano Ferne","ÇOFJSEDLFÇMQLWRNF","34991662346","Rua Sinne"),
                    Contact("Fulano Silky","ODJASJDOIUJRQWR","34991662346","Rua Flanny"),
                    Contact("Fulano Rafael","PKFJEHRJNAFFF","34991662346","Rua Soni"),
                    Contact("Fulano Neto","OIETJHLSKMNDVN","34991662346","Rua Flinstons"),
                    Contact("Fulano Orvalho","fgpsjepgflskdvmnlnd","34991662346","Rua Teste"),
                    Contact("Fulano Carvalho","ligsfhedlifnfvcsc","34991662346","Rua Boa vista"),
                    Contact("Fulano Teste","golishklfdfkmdsv","34991662346","Rua Grabber"),
                    Contact("Fulano Cansei","fliajhslkfndnvmhnb","34991662346","Rua jurupinga"),

                )
                ContactList(contacts = contactList){ contact ->
                    contactDisplayed = contact
                }
            }
        }
    }
}

@Composable
fun ProfileCard(contact: Contact) {
    var expandDetais by remember { mutableStateOf( false) }
    Card (
        Modifier
            .fillMaxWidth()
            .padding(4.dp)
            .padding(16.dp)
            ) {
        Column (
            Modifier
                .fillMaxWidth()
                .padding(vertical = 4.dp)
                ){
            ImageCard(100.dp ,Modifier.align(Alignment.CenterHorizontally))
            Row (
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
            ){
                Text(
                    text = contact.name,
                    textAlign = TextAlign.Center,
                    fontSize = 22.sp,
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1,
                    modifier = Modifier.widthIn(0.dp, 250.dp)
                )
                Text(
                    text = if (expandDetais) " – " else " + ",
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold,
                    color = Color(0xFF7A7A7A),
                    fontSize = 24.sp,
                    modifier = Modifier.clickable {
                        expandDetais = !expandDetais
                    }
                )
            }
            AnimatedVisibility(
                exit = fadeOut() + shrinkVertically(),
                visible = expandDetais,
                enter = fadeIn() + expandHorizontally()

            ) {
                Text(
                    text = stringResource(id = R.string.contact_description,
                        contact.name, contact.description,
                        contact.phone, contact.address
                    ))
            }
        }
    }
}

@Composable
fun ContactItemView(contact: Contact, onClick: () -> Unit){
    Card(
        elevation = 2.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(2.dp)
            .clickable { onClick() }
    ) {
        Row {
           ImageCard(size = 50.dp)
           Text(
               text = contact.name,
               modifier = Modifier.align(Alignment.CenterVertically),
               overflow = TextOverflow.Ellipsis,
               maxLines = 1
               )
        }
    }
}

@Composable
fun ContactList(contacts: List<Contact>, onClick:(contact: Contact)->Unit){
    LazyColumn{
        items(contacts){ contact ->
            ContactItemView(contact = contact){
                onClick(contact)
            }
        }
    }
}

@Composable
fun ImageCard(size: Dp, modifier: Modifier = Modifier) {
    Image(
        painter = painterResource(id = R.drawable.marco),
        contentDescription = stringResource(
            id = R.string.profile_picture
        ),
        modifier
            .clip(CircleShape)
            .size(size)
            .border(
                0.5.dp,
                Color(0xFFC2BBFF),
                CircleShape
            ),
        contentScale = ContentScale.Crop,
        )
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    BuildLayout()
}