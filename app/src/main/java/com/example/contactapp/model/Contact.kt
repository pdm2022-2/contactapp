package com.example.contactapp.model

data class Contact (
    var name:String = "",
    var description: String = "",
    var phone:String = "",
    var address: String = "",
    var id: Long = 0L
        )